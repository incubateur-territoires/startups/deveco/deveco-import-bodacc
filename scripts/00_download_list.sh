if [ ! -d "input/$1" ]
then
		echo "Création du dossier input/$1"
		mkdir -p input/$1
fi

echo "Téléchargement de la liste des fichiers pour l'année $1 vers input/$1/$1.html"

wget https://echanges.dila.gouv.fr/OPENDATA/BODACC/$1/ -O input/$1/$1.html
