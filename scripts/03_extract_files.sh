if [ ! -d "input/$1/xml" ]
then
		echo "Création du dossier input/$1/xml"
		mkdir input/$1/xml
fi

cd input/$1/xml

echo "Décompression des fichiers .taz pour l'année $1"

for i in $(ls ../compresse/*.taz) ; do tar -xvf $i; done

echo "Décompression des fichiers .tar pour l'année $1"

for i in $(ls ../compresse/*.tar) ; do tar -xvf $i; done

echo "Décompression des fichiers .zip pour l'année $1 vers input/$1/xml"

for i in $(ls ../compresse/*.zip) ; do unzip $i; done

cd ../../
