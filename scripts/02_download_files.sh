if [ ! -f "input/$1/$1_files.txt" ]
then
		echo "Le fichier input/$1/$1.html n'existe pas, générer grâce au script 00"
		exit 1
fi

if [ ! -d "input/$1/archives" ]
then
		echo "Téléchargement des fichiers archives pour l'année $1 vers input/$1/archives"
		mkdir "input/$1/archives"
fi

# 20XX-2023
# for i in $(cat $1); do wget "https://echanges.dila.gouv.fr/OPENDATA/BODACC/FluxHistorique/$2/$i"; done

# 2024
for i in $(cat input/$1/$1_files.txt);
do
		echo "Téléchargement du fichier $i pour l'année input/$1 vers input/$1/archives"
		wget "https://echanges.dila.gouv.fr/OPENDATA/BODACC/$1/$i" -O input/$1/archives/$i -o /dev/null
done
