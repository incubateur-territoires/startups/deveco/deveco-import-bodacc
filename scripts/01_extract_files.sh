if [ ! -f "input/$1/$1.html" ]
then
		echo "Le fichier input/$1/$1.html n'existe pas, générer grâce au script 00"
		exit 1
fi

echo "Extraction de la liste des fichiers pour l'année $1 vers input/$1/$1_files.txt"

less input/$1/$1.html | grep -oE 'PCL_[^<"]+' | uniq > input/$1/$1_files.txt
