## Deveco Import BODACC

Projet d'import des procédures collectives du BODACC

Source : [https://echanges.dila.gouv.fr/OPENDATA/BODACC](https://echanges.dila.gouv.fr/OPENDATA/BODACC)

### Installation

```bash
yarn
```

### Usage

```bash
bash scripts/00_download_list.sh 2024

bash scripts/01_extract_files.sh 2024

bash scripts/02_download_files.sh 2024

bash scripts/03_extract_files.sh 2024

node src/index.js input
```
