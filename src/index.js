const fs = require("fs");
const path = require("path");
const { XMLParser } = require("fast-xml-parser");

const main = async () => {
  const rootdirPath = process.argv[2];
  if (!rootdirPath) {
    console.error(`usage: node src/index.js chemin/vers/dossier/input`);
    process.exit(1);
  }

  const dirPaths = await fs.promises.readdir(rootdirPath);

  const parser = new XMLParser();

  if (!fs.existsSync("output")) {
    fs.mkdirSync("output");
  }

  const writeStream = fs.createWriteStream(
    "output/bodacc-procedure-collective.csv"
  );

  writeStream.write(`siren,famille,nature,date\n`);

  for await (const dirPath of dirPaths) {
    console.log(dirPath);

    const xmlDirPath = path.join(dirPath, "xml");

    const filePaths = await fs.promises.readdir(
      path.join(rootdirPath, xmlDirPath)
    );

    for await (const filePath of filePaths) {
      console.log(xmlDirPath, filePath);

      const file = await fs.promises.readFile(
        path.join(rootdirPath, xmlDirPath, filePath)
      );

      const xml = parser.parse(file);

      const annonces = xml.PCL_REDIFF.annonces.annonce;

      for await (const annonce of annonces) {
        try {
          if (!annonce.numeroImmatriculation) continue;

          const {
            numeroAnnonce,
            numeroImmatriculation: { numeroIdentificationRCS },
            jugement,
          } = annonce;

          if (!numeroIdentificationRCS || !jugement) continue;

          let { famille, nature, date } = jugement;

          const siren = numeroIdentificationRCS.replace(/ /g, "");

          if (siren === "000000000") {
            continue;
          }

          if (!date) {
            date = "";
          }

          writeStream.write(`${siren},${famille},${nature},${date}`);
          writeStream.write(`\n`);
        } catch (e) {
          console.error(e, annonce);

          process.exit(1);
        }
      }
    }
  }
};

main().catch(console.error);
